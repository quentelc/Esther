#!/usr/bin/env python
# -*- coding: utf-8

#!/usr/bin/env python
# -*- coding: utf-8 -*-


######################################################################
#                                                                    #
#                            Esther                                  #
#                                                                    #
#  Ebauche d'un système de topologie heuristique des Etats en réseau #
#                                                                    #
#                     sql_nomenclatures.py                           #
#                                                                    #
#                     sept. 2018 - CQ et LD                          #
#                                                                    #
######################################################################


# Ce script crée une base SQLite 3 dans laquelle nous stockerons les nomenclatures utiles
#  à l'application
#
#
# SCRIPT
#
# 1. IMPORTATION DES MODULES NECESSAIRES

import os
import pandas as pd
from fonctions.utils import enregistrer

#Variable globale du chemin du repertoire du programme
repertoire = os.path.dirname(os.path.abspath(__file__))
while os.path.basename(repertoire) != "Esther":
    repertoire = os.path.dirname(repertoire)

repertoire_nomenclatures = repertoire +'/nomenclatures/'

# Table des noms de pays
noms_pays_csv = repertoire_nomenclatures+"noms_pays.csv"

# Attention ! Pour que la Namibie (iso : na) ne se retrouve pas avec un code nul, il faut le préciser dans le read...
data = pd.read_csv(noms_pays_csv, sep=';', keep_default_na=False, na_values=[''])
enregistrer(data, "nomenclatures", "noms_pays", effacer_ancien = False, if_exists = 'replace')