#!/usr/bin/env python
# -*- coding: utf-8

######################################################################
#                                                                    #
#                            Esther                                  #
#                                                                    #
#  Ebauche d'un système de topologie heuristique des Etats en réseau #
#                                                                    #
#                         import_AGNU.py                             #
#                                                                    #
#                     sept. 2018 - CQ et LD                          #
#                                                                    #
######################################################################


import os
import sys

from sqlalchemy.engine import create_engine

import numpy as np
import pandas as pd
import xarray
from sqlalchemy.engine import create_engine
import numpy as np
import pandas as pd
import xarray as xr


sys.path.insert(0, os.path.abspath('../..'))
sys.path.insert(0, os.path.abspath('..'))



#Variable globale du chemin du repertoire du programme
repertoire = os.path.dirname(os.path.abspath(__file__))
while os.path.basename(repertoire) != "Esther":
    repertoire = os.path.dirname(repertoire)
repertoire = repertoire +'/donnees/'

def chemin_base(nom):
    return repertoire+'bases/'+nom+'.db'


# Fonctions utiles à notre application

def enregistrer(data, nom, table, effacer_ancien = False, if_exists = 'replace'):
    db = chemin_base(nom)
    if(not(os.path.exists(db))):
        os.mknod(db)
    elif(effacer_ancien):
        os.remove(db)
        os.mknod(db)
    conn = create_engine('sqlite:///'+db, echo=False)
    data.to_sql(table, conn, index = False, if_exists = if_exists)