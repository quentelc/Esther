#!/usr/bin/env python
# -*- coding: utf-8

#!/usr/bin/env python
# -*- coding: utf-8 -*-


######################################################################
#                                                                    #
#                            Esther                                  #
#                                                                    #
#  Ebauche d'un système de topologie heuristique des Etats en réseau #
#                                                                    #
#                        nomenclatures.py                            #
#                                                                    #
#                     sept. 2018 - CQ et LD                          #
#                                                                    #
######################################################################


# Fonctions Pandas sur les nomenclatures.

import os
import unidecode

from sqlalchemy.engine import create_engine

import pandas as pd


#Variable globale du chemin du repertoire du programme
repertoire = os.path.dirname(os.path.abspath(__file__))
while os.path.basename(repertoire) != "Esther":
    repertoire = os.path.dirname(repertoire)
repertoire = repertoire +'/donnees/'

def chemin_base(nom):
    return repertoire+'bases/'+nom+'.db'

path = chemin_base("nomenclatures")
conn = create_engine('sqlite:///'+path,  echo= False)

"""
Transforme la colonne 'iso3' de data en 'iso2nation'
"""
def de_iso3_vers_iso2(data):
    pays = pd.read_sql_table('noms_pays',conn, columns=['iso3', 'iso2nation'])
    pays.columns = ['iso3','iso2nation']
    return pd.merge(data, pays, on='iso3').drop('iso3', axis=1)

"""
Sors une liste de toutes les nations
"""
def liste_iso():
    pays = pd.read_sql_table('noms_pays',conn, columns=['iso2nation'])
    pays.columns = ['iso']
    return pays.drop_duplicates()

"""
Ajoute une colonne avec les noms en français à data qui contient une colonne 'iso3'
"""
def avec_iso_ajouter_nom_fr(data):
    pays = pd.read_sql_table('noms_pays',conn, columns=['pays_fr', 'iso2'])
    pays.columns = ['pays_fr','iso']
    pays = pays.drop_duplicates('iso')
    return pd.merge(data, pays, on='iso', how = 'left')
