#!/usr/bin/python3
# -*- coding: utf-8 -*-

########################################
#                                      #
#               ESTHER                 #
#   Ebauche d'un système de topologie  #
#  heuristique sur les Etats en réseau #
#                                      #
#    analysis/opentrade_analysis.py    #
#                                      #
########################################

"""
Ce script analyse les matrices d'adjacence annuel de commerce entre les nations récupérées par notre script d'input.
"""

import os
import pandas as pd
from datetime import datetime
from utils import load_result, find_script_pwd
from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
import geopandas as gpd
from math import pi
import imageio

# Cette fonction dessine un graphique par année, où la position de tous les pays partenaires par rapport
# à un pays central est mesurée en étoile
def edge_analysis_one_country():
    # La liste des pays sur lesquels je souhaite faire porter mon analyse
    countries_list = ["AO", "BJ", "BW", "BF", "BI", "CM", "CV", "CF", "TD", "KM", "CG", "CD", "CI", "DJ", "GQ",
                     "ER", "ET", "GA", "GM", "GH", "GN", "GW", "KE", "LS", "LR", "MG", "MW", "ML", "MR", "MU", "MZ",
                     "NA", "NE", "NG", "RW", "ST", "SN", "SC", "SL", "SO", "ZA", "SD", "SZ", "TZ", "TG", "UG",
                     "ZM", "ZW"]
    center_country = "CN"
    center_country_name_with_article = "de la Chine"
    this_year = datetime.now().year
    # L'année où nous allons commencer notre analyse
    year = 1962
    data = load_result("opentrade")
    # Supprimons les colonnes qui ne nous intéressent pas ici
    data.drop(['import_value', 'export_value'], axis = 1, inplace = True)

    # Trouvons le total annuel des échanges entre chaque pays rapporteur et l'ensemble des pays du monde
    annual_total = data.groupby(['year', 'reporter_iso'])[['exchange_value']].sum()
    annual_total = annual_total.reset_index().rename(columns={'exchange_value': 'total_exchanges_by_year'})

    # Ajoutons le total annuel à chaque ligne de notre df principale
    data = pd.merge(data, annual_total, on=['reporter_iso', 'year'], how= "left")

    # Isolons les échanges par an avec le pays central
    center_country_df = data.loc[data['partner_iso'] == center_country]
    center_country_df.drop("partner_iso", axis=1, inplace = True)
    center_country_df = center_country_df.reset_index()
    center_country_df.drop("index", axis=1, inplace=True)

    # Calculons le pourcentage des échanges avec le pays centre dans le total des échanges commerciaux des pays
    # rapporteurs
    center_country_df['pc_center_country'] = (center_country_df['exchange_value']/center_country_df['total_exchanges_by_year'])*100
    center_country_df.drop(['exchange_value', 'total_exchanges_by_year'], axis =1, inplace = True)
    center_country_df.fillna(0, inplace = True)
    # Si nous ne nous intéressons qu'à certains pays, filtrons
    subset = center_country_df.loc[center_country_df["reporter_iso"].isin(countries_list)]
    # Normalisons. La valeur maximale des échanges sera à 100
    max = subset['pc_center_country'].max()
    subset['pc_center_country'] = subset['pc_center_country']*(100/max)

    # Dessinons maintenant un graphique par année
    year_2 = year
    while year_2 < this_year:
        # Ne sélectionnons que les données de l'année en question
        year_df = subset[subset["year"] == year_2].reset_index()
        year_df.drop(['index', 'year'], axis = 1, inplace = True)
        cat = year_df['reporter_iso'].tolist()
        values = year_df['pc_center_country'].tolist()

        # Dessinons
        N = len(cat)

        x_as = [n / float(N) * 2 * pi for n in range(N)]

        # Because our chart will be circular we need to append a copy of the first
        # value of each list at the end of each list with data
        values += values[:1]
        x_as += x_as[:1]

        # Set color of axes
        plt.rc('axes', linewidth=0.5, edgecolor="#888888")

        # Create polar plot
        ax = plt.subplot(111, polar=True)

        # Set clockwise rotation. That is:
        ax.set_theta_offset(pi / 2)
        ax.set_theta_direction(-1)

        # Set position of y-labels
        ax.set_rlabel_position(0)

        # Set color and linestyle of grid
        ax.xaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.1)
        ax.yaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.3)

        # Set number of radial axes and remove labels
        plt.xticks(x_as[:-1], [])

        # Précisons l'échelle
        plt.yticks([20, 40, 60, 80], ["20%", "40%", "60%", "80%"], fontsize=5, fontstyle='italic')

        # Plot data
        ax.plot(x_as, values, linewidth=0, linestyle='solid', zorder=3)

        # Fill area
        ax.fill(x_as, values, 'r', alpha=0.3)

        # Set axes limits
        plt.ylim(0, 100)

        # Dessine proprement le code ISO des pays sur le pourtour du radar
        for i in range(N):
            angle_rad = i / float(N) * 2 * pi

            if angle_rad == 0:
                ha, distance_ax = "center", 2.5
            elif 0 < angle_rad < pi:
                ha, distance_ax = "left", 2.5
            elif angle_rad == pi:
                ha, distance_ax = "center", 2.5
            else:
                ha, distance_ax = "right", 2.5

            ax.text(angle_rad, 100 + distance_ax, cat[i], size=6, horizontalalignment=ha, verticalalignment="center")

        # Ajoutons l'année
        ax.text(0, 0, str(year_2), fontsize=10, fontweight='bold', horizontalalignment='center',
                verticalalignment = 'center', transform = ax.transAxes)

        # Ajoutons un titre
        plt.title("Part "+center_country_name_with_article+" dans les échanges commerciaux", fontsize=12, fontweight='bold',
                  verticalalignment='center', horizontalalignment='center', transform = ax.transAxes)

        # Dessinons et/ou enregistrons
        list_to_join = [find_script_pwd(), "data/figs/", str(year_2) + ".png"]
        png_path = os.path.join(*list_to_join)
        plt.savefig(png_path, dpi=1000)
        # Nettoyons la figure actuelle
        plt.clf()

        # Et on passe à l'année suivante
        year_2 = year_2 + 1

    # Créons une animation avec le résultat

    png_dir = os.path.join(find_script_pwd(), "data/figs/")
    images = []
    for file_name in sorted(os.listdir(png_dir)):
        if file_name.endswith('.png'):
            file_path = os.path.join(png_dir, file_name)
            images.append(imageio.imread(file_path))
    gif_path = os.path.join(png_dir, 'fig.gif')
    imageio.mimsave(gif_path, images, subrectangles=True)

def edge_analysis_two_countries():
    # La liste des pays sur lesquels je souhaite faire porter mon analyse
    countries_list = ["AO", "BJ", "BW", "BF", "BI", "CM", "CV", "CF", "TD", "KM", "CG", "CD", "CI", "DJ", "GQ",
                     "ER", "ET", "GA", "GM", "GH", "GN", "GW", "KE", "LS", "LR", "MG", "MW", "ML", "MR", "MU", "MZ",
                     "NA", "NE", "NG", "RW", "ST", "SN", "SC", "SL", "SO", "ZA", "SD", "SZ", "TZ", "TG", "UG",
                     "ZM", "ZW"]
    center_country_1 = "FR"
    center_country_2 = "CN"
    center_countries_names_with_articles = "de la Chine"
    this_year = datetime.now().year
    # L'année où nous allons commencer notre analyse
    year = 1962
    data = load_result("opentrade")
    # Supprimons les colonnes qui ne nous intéressent pas ici
    data.drop(['import_value', 'export_value'], axis = 1, inplace = True)

    # Trouvons le total annuel des échanges entre chaque pays rapporteur et l'ensemble des pays du monde
    annual_total = data.groupby(['year', 'reporter_iso'])[['exchange_value']].sum()
    annual_total = annual_total.reset_index().rename(columns={'exchange_value': 'total_exchanges_by_year'})

    # Ajoutons le total annuel à chaque ligne de notre df principale
    data = pd.merge(data, annual_total, on=['reporter_iso', 'year'], how= "left")

    # Ne gardons que les pays centraux
    data = data.loc[data['partner_iso'].isin([center_country_1, center_country_2])]
    # Mettons en colonnes nos valeurs par pays centraux
    data[center_country_1] = 0
    data[center_country_2] = 0

    data.loc[data['partner_iso'] == center_country_1, center_country_1] = data['exchange_value']
    data.loc[data['partner_iso'] == center_country_2, center_country_2] = data['exchange_value']
    data.drop(['partner_iso', 'exchange_value'], axis=1, inplace = True)
    data2 = data.groupby(["reporter_iso", "year"])[[center_country_1, center_country_2]].sum()
    data2 = data2.reset_index()
    data.drop([center_country_1, center_country_2], axis=1, inplace = True)
    data.drop_duplicates(keep="first", inplace=True)
    data = pd.merge(data2, data, on = ['reporter_iso', 'year'], how = 'outer' )

    # Si, pour une année donnée, les échanges avec le pays sont supérieurs aux échanges avec le pays 2, nous fixons la
    # valeur des échanges pour le pays 2 à 0
    # data.loc[data[center_country_1] > data[center_country_2], center_country_2] = 0
    # data.loc[data[center_country_2] > data[center_country_1], center_country_1] = 0

    # Calculons le pourcentage des échanges avec les deux pays centraux
    data["pc_country_1"] = (data[center_country_1]/data['total_exchanges_by_year'])*100
    data["pc_country_2"] = (data[center_country_2] / data['total_exchanges_by_year']) * 100
    data.drop(['total_exchanges_by_year', center_country_2, center_country_1], axis = 1, inplace = True)

    #  Si nous ne nous intéressons qu'à certains pays, filtrons
    subset = data.loc[data["reporter_iso"].isin(countries_list)]

    # Dessinons une carte par année
    year_2 = year
    while year_2 < this_year:
        # Ne sélectionnons que les données de l'année en question
        year_df = subset[subset["year"] == year_2].reset_index()
        year_df.drop(['index', 'year'], axis = 1, inplace = True)
        year_df.columns = ['iso', center_country_1, center_country_2]
        year_df[center_country_1].fillna(0, inplace=True)
        year_df[center_country_2].fillna(0, inplace=True)
        # Ajoutons un pays à 0, et un pays à 100
        year_df.loc[-1] = ['X1', 0, 100]
        year_df.loc[-2] = ['X2', 100, 0]
        year_df = year_df.reset_index()
        year_df.drop("index", axis = 1, inplace = True)


        # #  Ne sélectionnons que les données de l'année en question
        # year_2df = subset2[subset2["year"] == year_2].reset_index()
        # year_2df.drop(['index', 'year'], axis=1, inplace=True)
        # year_2df.columns = ['iso', 'value']
        # #  Ajoutons un pays à 0, et un pays à 100
        # year_2df.loc[-1] = ['X1', 0]
        # year_2df.loc[-1] = ['X2', 100]

        # On a : longitude ouest, latitude sud, longitude est, latitude nord
        africa = Polygon([(-17,-34),(55,-34),(55,30),(-17,30)])

        map_path = os.path.join(find_script_pwd(), "data/ressources/cartographie/pays.shp")
        world = gpd.read_file(map_path)[['ISO', 'geometry']]
        world.columns = ['iso', 'geometry']
        # Sur notre shape aussi, ne retenons que les pays de notre subset
        world = world.loc[world["iso"].isin(countries_list)]

        world['geometry'] = world['geometry'].apply(lambda x: x.intersection(africa))

        world_with_data = pd.merge(world, year_df, on='iso', how='outer')

        world_with_data.fillna(0, inplace=True)
        world_with_data[center_country_1] = world_with_data[center_country_1].astype(float)
        world_with_data[center_country_2] = world_with_data[center_country_2].astype(float)

        cmap1 = 'Blues'
        cmap2 = 'Reds'
        fg, ax = plt.subplots(1)
        fg.set_size_inches(18.91, 12.78)

        fg.set_dpi(500)

        #ax = world_with_data.plot(column=center_country_1, cmap=cmap1, ax=ax, linewidth=0.8, edgecolor='0.8')
        ax = world_with_data.plot(column=center_country_2, cmap=cmap2, ax=ax, linewidth=0.8, edgecolor='0.8')

        # Ajoutons un titre
        plt.title("\nParts "+center_countries_names_with_articles+" dans les échanges commerciaux", fontsize=18, fontweight='bold',
                  verticalalignment='bottom', horizontalalignment='center', transform = ax.transAxes)

        #  Ajoutons l'année
        ax.text(0.1, 0.1, str(year_2), fontsize=14, fontweight='bold', horizontalalignment='center',
                verticalalignment='center', transform=ax.transAxes)

        ax.set_axis_off()
        this_map_path = os.path.join(find_script_pwd(), "data/maps/"+str(year_2)+".png")
        fg.savefig(this_map_path, bbox_inches='tight', pad_inches=0)
        plt.close()

        # Et on passe à l'année suivante
        year_2 = year_2 + 1

    # Créons une animation avec le résultat

    png_dir = os.path.join(find_script_pwd(), "data/maps/")
    images = []
    for file_name in sorted(os.listdir(png_dir)):
        if file_name.endswith('.png'):
            file_path = os.path.join(png_dir, file_name)
            images.append(imageio.imread(file_path))
    gif_path = os.path.join(png_dir, 'map.gif')
    imageio.mimsave(gif_path, images, subrectangles=True)


if __name__ == "__main__":
    #edge_analysis_one_country()
    edge_analysis_two_countries()