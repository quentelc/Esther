#!/usr/bin/python3
# -*- coding: utf-8 -*-

########################################
#                                      #
#               ESTHER                 #
#   Ebauche d'un système de topologie  #
#  heuristique sur les Etats en réseau #
#                                      #
#         analysis/utils.py            #
#                                      #
########################################

"""
Ce script regroupe les fonctions utiles à plusieurs de nos modules d'importation.
"""

import os
import pandas as pd

#1. find_script_pwd()
# Cette fonction trouve le chemin du présent script

def find_script_pwd():
    app_pwd = os.path.dirname(os.path.abspath(__file__))
    while os.path.basename(app_pwd) != "Esther":
        app_pwd = os.path.dirname(app_pwd)
    return app_pwd

# 2. load_result
# Une fonction simple pour charger un fichier .csv de résultats dans une dataframe
def load_result(nom_ressource, skip_rows = 0,encoding="UTF-8", usecols = None, sep=',', quotechar='\"'):
    list_to_join = [find_script_pwd(), "data/raw_data/", nom_ressource+".csv"]
    csv_path = os.path.join(*list_to_join)
    #  Attention ! A l'importation, je dois bien préciser "na_values= "NaN", keep_default_na= False" pour que le
    #  le code pays de la Namibie n'apparaisse pas comme un NaN...
    return pd.read_csv(csv_path, encoding=encoding, sep=sep, skiprows = skip_rows, usecols = usecols,
                       quotechar=quotechar, na_values="NaN", keep_default_na=False, low_memory=False)
