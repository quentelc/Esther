#!/usr/bin/env python
# -*- coding: utf-8


######################################################################
#                                                                    #
#                            Esther                                  #
#                                                                    #
#  Ebauche d'un système de topologie heuristique des Etats en réseau #
#                                                                    #
#                           Esther.py                                #
#                                                                    #
#                     sept. 2018 - CQ et LD                          #
#                                                                    #
######################################################################


# Module principal d'Esther

# import sys, os
from input.import_AGNU import load_AGNU
from input.import_AGNU_par_pays import load_AGNU_par_pays

# sys.path.insert(0, os.path.abspath('../..'))
# sys.path.insert(0, os.path.abspath('..'))

if __name__ == "__main__":
    load_AGNU()
    load_AGNU_par_pays()
