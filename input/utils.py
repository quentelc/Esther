#!/usr/bin/python3
# -*- coding: utf-8 -*-

########################################
#                                      #
#               ESTHER                 #
#   Ebauche d'un système de topologie  #
#  heuristique sur les Etats en réseau #
#                                      #
#          input/utils.py              #
#                                      #
########################################

"""
Ce script regroupe les fonctions utiles à plusieurs de nos modules d'importation.
"""

import os
import webdav3.client as wd
import pandas as pd

#1. find_script_pwd()
# Cette fonction trouve le chemin du présent script

def find_script_pwd():
    app_pwd = os.path.dirname(os.path.abspath(__file__))
    while os.path.basename(app_pwd) != "Esther":
        app_pwd = os.path.dirname(app_pwd)
    return app_pwd

# 2. load_accreditation
def load_accreditation(ressource_name):
  app_pwd = find_script_pwd()
  config_path = os.path.join(app_pwd, "data/config/config.csv")
  data = pd.read_csv(config_path)
  accreditation = data.loc[data['ressource'] == ressource_name].reset_index()
  login = accreditation.loc[0]['login']
  password = accreditation.loc[0]['password']
  address = accreditation.loc[0]['address']
  return login, password, address

# 3. pull_from_the_cloud
# Récupère un fichier sur notre cloud, et le stocke dans le dossier "data/ressources"
# ATTENTION : ici, il nous faut le webdav_root.
def pull_from_the_cloud(module_name):
    list_to_join = [find_script_pwd(), "data/ressources", module_name + ".csv"]
    module_result_path = os.path.join(*list_to_join)
    remote_file_path = os.path.join("ouranos", module_name+".csv")
    accred = load_accreditation("nextcloud")
    options = {
        'webdav_hostname' : accred[2],
        'webdav_login' : accred[0],
        'webdav_password': accred[1],
        'webdav_root': '/nextcloud/remote.php/webdav/'
    }
    client = wd.Client(options)
    # Ce fichier est-il sur mon cloud ?
    if client.check(remote_file_path) == True:
        client.download_sync(remote_path=remote_file_path, local_path=module_result_path)
    else:
        print("Le fichier ", remote_file_path, "n'existe pas sur notre cloud.")

# 4.load_result
# Une fonction simple pour charger un fichier .csv de résultats dans une dataframe
def load_result(nom_ressource, skip_rows = 0,encoding="UTF-8", usecols = None, sep=',', quotechar='\"'):
    list_to_join = [find_script_pwd(), "data/raw_data/", nom_ressource+".csv"]
    csv_path = os.path.join(*list_to_join)
    #  Attention ! A l'importation, je dois bien préciser "na_values= "NaN", keep_default_na= False" pour que le
    #  le code pays de la Namibie n'apparaisse pas comme un NaN...
    return pd.read_csv(csv_path, encoding=encoding, sep=sep, skiprows = skip_rows, usecols = usecols,
                       quotechar=quotechar, na_values="NaN", keep_default_na=False, low_memory=False)

# 5. save_raw_data
# Sauvegarder au format .csv un fichier de résultats
def save_raw_data(data, file_name_without_extension):
    list_to_join = [find_script_pwd(), "data/raw_data", file_name_without_extension + ".csv"]
    csv_path = os.path.join(*list_to_join)
    data.to_csv(csv_path, index=False)
    return