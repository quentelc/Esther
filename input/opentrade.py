#!/usr/bin/python3
# -*- coding: utf-8 -*-

########################################
#                                      #
#               ESTHER                 #
#   Ebauche d'un système de topologie  #
#  heuristique sur les Etats en réseau #
#                                      #
#        input/opentrade.py            #
#                                      #
########################################

"""
Ce script récupère les échanges commerciaux internationaux par pays collectés auprès d'Opentradestatistics et mis en
forme de matrices d'adjacence annuelles par notre application Ouranos
"""

import os
import pandas as pd
from pathlib import Path
from datetime import datetime
from utils import pull_from_the_cloud, find_script_pwd, load_result, save_raw_data

nation_codes = [ "AD", "AE", "AF", "AG", "AL", "AM", "AO", "AR", "AT", "AU", "AZ", "BA", "BB", "BD", "BE", "BF", "BG",
                "BH", "BI", "BJ", "BN", "BO", "BR", "BS", "BT", "BW", "BY", "BZ", "CA", "CD", "CF", "CG", "CH", "CI",
                "CK", "CL", "CM", "CN", "CO", "CR", "CS", "CU", "CV", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ",
                "EC", "EE", "EG", "ER", "ES", "ET", "FI", "FJ", "FM", "FR", "GA", "GB", "GE", "GH", "GM", "GN", "GQ",
                "GR", "GT", "GW", "GY", "HN", "HR", "HT", "HU", "ID", "IE", "IL", "IN", "IQ", "IR", "IS", "IT", "JM",
                "JO", "JP", "KE", "KG", "KH", "KI", "KM", "KN", "KP", "KR", "KW", "KZ", "LA", "LB", "LC", "LI", "LK",
                "LR", "LS", "LT", "LU", "LV", "LY", "MA", "MC", "MD", "ME", "MG", "MH", "MK", "ML", "MM", "MN", "MR",
                "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NE", "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ",
                "OM", "PA", "PE", "PG", "PH", "PK", "PL", "PS", "PT", "PW", "PY", "QA", "RO", "RS", "RU", "RW", "SA",
                "SB", "SC", "SD", "SE", "SG", "SI", "SK", "SL", "SM", "SN", "SO", "SR", "SS", "ST", "SV", "SY", "SZ",
                "TD", "TG", "TH", "TJ", "TL", "TM", "TN", "TO", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "US", "UY",
                "UZ", "VA", "VC", "VE", "VN", "VU", "WS", "XK", "YE", "ZA", "ZM", "ZW"]

def load_opentrade():
    # Ouranos a produit une matrice Opentrade par année. Ces fichiers s'appellent opentrade_matrix_import_YYYY.csv et
    # opentrade_matrix_export_YYYY.csv. La base remonte à 1962 au maximum, et s'arrête à l'année dernière dans le
    # meilleur des cas. Je peux tenter de télécharger tous les fichiers correspondants (si ça rate, c'est qu'Ouranos
    # n'a pas créé les fichiers de l'année considérée, et ce n'est pas un drame). J'en fais ensuite un fichier unique.
    this_year = datetime.now().year
    year = 1962
    opentrade = pd.DataFrame(columns=['year','reporter_iso','partner_iso','export_value','import_value','exchange_value'])
    while year < this_year:
        print("Je traite l'année ", year)
    #     export_file_name = "opentrade_matrix_export_%s" % str(year)
    #     pull_from_the_cloud("export_file_name")
    #     #  Le chemin du fichier que nous venons de télécharger
    #     ouranos_gift = os.path.join(find_script_pwd(), "datas/ressources/"+export_file_name+".csv")
    #     #  Le chemin du fichier que nous allons créer
    #     ouranos_transmo = os.path.join(find_script_pwd(), "datas/raw_data/"+export_file_name+"csv")
    #     if Path(ouranos_gift).is_file():
    #         data = pd.read_csv(ouranos_gift, na_values="NaN", keep_default_na=False)
    #         data.to_csv(ouranos_transmo, index=False)
    #         os.remove(ouranos_gift)
    #     else:
    #         print("Pas de fichier %s" % export_file_name)
    #     import_file_name = "opentrade_matrix_import_%s" % str(year)
    #     pull_from_the_cloud("import_file_name")
    #     #  Le chemin du fichier que nous venons de télécharger
    #     ouranos_gift = os.path.join(find_script_pwd(), "datas/ressources/" + import_file_name + ".csv")
    #     #  Le chemin du fichier que nous allons créer
    #     ouranos_transmo = os.path.join(find_script_pwd(), "datas/raw_data/" + import_file_name + "csv")
    #     if Path(ouranos_gift).is_file():
    #         data = pd.read_csv(ouranos_gift, na_values="NaN", keep_default_na=False)
    #         data.to_csv(ouranos_transmo, index=False)
    #         os.remove(ouranos_gift)
    #     else:
    #         print("Pas de fichier %s" % import_file_name)

        # Je compile ensuite le tout en un seul fichier opentrade.csv
        this_year_export_matrix = load_result("opentrade_matrix_export_%s" % str(year))
        this_year_import_matrix = load_result("opentrade_matrix_import_%s" % str(year))
        # Plutôt que de pivoter, je vais iterrer
        this_year_export_df = pd.DataFrame()
        this_year_import_df = pd.DataFrame()
        for index, row in this_year_export_matrix.iterrows():
            reporter = row["reporter_iso"]
            print('Je traite le rapporteur :', reporter)
            for nation in nation_codes:
                export_val = row[nation]
                new_row = {'year': year, 'reporter_iso': reporter, 'partner_iso': nation, 'export_value': export_val}
                this_year_export_df = this_year_export_df.append(new_row, ignore_index=True)
        this_year_import_df = pd.DataFrame()
        for index, row in this_year_import_matrix.iterrows():
            reporter = row["reporter_iso"]
            print('Je traite le rapporteur :', reporter)
            for nation in nation_codes:
                import_val = row[nation]
                new_row = {'year': year, 'reporter_iso': reporter, 'partner_iso': nation, 'import_value': import_val}
                this_year_import_df = this_year_import_df.append(new_row, ignore_index=True)
        # Fusionnons importations et exportations
        this_year_df = pd.merge(this_year_export_df, this_year_import_df, on = ['reporter_iso', 'partner_iso', 'year'])
        this_year_df['year'] = this_year_df['year'].astype(int)
        this_year_df['export_value'] = this_year_df['export_value'].astype(float)
        this_year_df['import_value'] = this_year_df['import_value'].astype(float)
        this_year_df['exchange_value'] = this_year_df['export_value']+this_year_df['import_value']
        opentrade = opentrade.append(this_year_df)
        save_raw_data(opentrade, "opentrade")
        print(opentrade.shape)
        year = year + 1

load_opentrade()

# if __name__ == "__main__":
#     load_opentrade()