#!/usr/bin/env python
# -*- coding: utf-8 -*-

######################################################################
#                                                                    #
#                            Esther                                  #
#                                                                    #
#  Ebauche d'un système de topologie heuristique des Etats en réseau #
#                                                                    #
#                         import_AGNU.py                             #
#                                                                    #
#                     sept. 2018 - CQ et LD                          #
#                                                                    #
######################################################################


# Ce script télécharge la base de données des votes de tous les Etats
# pendant les 72 premières sessions de l'AGNU et crée une ou plusieurs
# matrice(s) de relations sur cette base

import os
import pandas as pd
from fonctions.nomenclatures import de_iso3_vers_iso2, avec_iso_ajouter_nom_fr, liste_iso

#Variable globale du chemin du repertoire du programme
repertoire = os.path.dirname(os.path.abspath(__file__))
while os.path.basename(repertoire) != "Esther":
    repertoire = os.path.dirname(repertoire)
repertoire_AGNU = repertoire +'/donnees/donnees_brutes/AGNU/'
repertoire_matrice = repertoire+'/donnees/donnees_exportees/'
fichier_AGNU = repertoire_AGNU+"UNdata.csv"

def load_AGNU():
    #  Nous pouvons traiter soit des années, soit des périodes
    temporalite = "annee"

    if temporalite == "annee":
        debut = 1946  # compris
        fin = 2019  # non-compris
        annee_en_cours = debut

    else:
        debut = 1946
        fin = 2019
        annee_en_cours = debut
        #  On fixe ici le pas de l'analyse (2 ans, 3 ans, 5 ans...)
        intervalle = 5

    while annee_en_cours < fin:
        # On lit le fichier source. A noter que l'encoding a changé en 2019, et que la seule façon de trouver le bon
        # est d'en tester plusieurs...
        data = pd.read_csv(fichier_AGNU, encoding = "ISO-8859-1")
        print(data.columns.values)

        # On ne retient du fichier source que les années qui nous intéressent
        if temporalite == "annee":
            data = data[(data['year'] == annee_en_cours)]
        else:
            debut_periode = annee_en_cours
            fin_periode = annee_en_cours+intervalle
            data = data[(data['year'] >= debut_periode) &(data['year'] < fin_periode)]

        # On n'a plus besoin de l'année : supprimons de la dataframe
        data = data.drop('year', axis =1)

        # Renommons la colonne "Countny" en "iso3"
        data = data.rename(columns={'Country':'iso3'})

        # Convertissons le code ISO3 en code ISO de la nation
        data = de_iso3_vers_iso2(data)

        # Et enfin, ne conservons que l'id du scrutin, le vote effectué et le code du pays
        data = data[['rcid', 'vote','iso2nation']]

        # Renommons notre colonne "iso2nation" en "iso"
        data = data.rename(columns={'iso2nation':'iso'})

        # Construisons une liste de tous les pays du monde avec leur nom en français
        # Cette liste nous servira ensuite de base pour construire une matrice avec tous les pays en absicisse
        # et en ordonnée, leur croisement correspondant au nombre moyen de votes en commun sur la période considérée
        liste_pays = liste_iso()

        # Mais supprimons de notre liste les pays qui ne votent pas à l'ONU
        liste_pays = liste_pays [~liste_pays ['iso'].isin(["XX","CK","VA","PS","XK","NU"])]

        # Et supprimons de notre liste les pays qui n'existaient pas encore au moment du vote
        if annee_en_cours < 1971:
            liste_pays = liste_pays [~liste_pays ['iso'].isin(["CN"])]
        else:
            liste_pays = liste_pays[~liste_pays['iso'].isin(["TW"])]
        if annee_en_cours < 2011:
            liste_pays = liste_pays [~liste_pays ['iso'].isin(["SS"])]
        if annee_en_cours < 2011:
            liste_pays = liste_pays [~liste_pays ['iso'].isin(["SS"])]
        if annee_en_cours < 2000:
            liste_pays = liste_pays [~liste_pays ['iso'].isin(["TL"])]

        total = avec_iso_ajouter_nom_fr(liste_pays)

        # Puis une autre
        noms = avec_iso_ajouter_nom_fr(liste_pays)

        # Traitons maintenant nos données pays par pays
        for _,row in noms.iterrows():
            # Construisons une dataframe avec tous les votes effectués par le pays concerné
            fr = data[data['iso'] == row['iso']].drop('iso', axis=1)
            # Construisons une seconde dataframe avec tous les votes qui n'ont pas été effectués par le pays concerné
            not_fr = data[data['iso'] != row['iso']]
            # Calculons le nombre de vote auxquels le pays a participé
            nb_res = float(fr.shape[0])
            # Croisons maintenant les deux dataframes pour trouver, dans celle qui regroupe les votes des autres,
            # les votes qui ont été identiques à ceux du pays concerné
            same_vote = pd.merge(not_fr,  fr,  on=['rcid',  'vote'])
            # Virons de notre dataframe sur les votes en commun l'identifiant des scrutins et le vote lui-même
            same_vote = same_vote.drop(['rcid', 'vote'], axis=1)
            # Créons maintenant une dataframe avec le nombre de vote commun de chaque pays avec le pays concerné
            same_vote = pd.DataFrame(same_vote['iso'].value_counts()).reset_index()
            # Redonnons maintenant au pays concerné son nom complet
            same_vote.columns = ['iso', row['pays_fr']]
            # Calculons maintenant le nombre moyen de scrutins sur lesquels notre pays a eu le même vote que ses partenaires
            same_vote[row['pays_fr']] = same_vote[row["pays_fr"]] / nb_res
            # Ajoutons maintenant nos conclusions sur ce pays à notre dataframe complète
            total = pd.merge(total, same_vote, on='iso', how='left')

        # A l'arrivée, nous avons un tableau
        # Virons les valeurs nulles
        total = total.dropna(axis = 1, how='all')
        total = total.dropna(axis = 0, how='all')

        # Virons la colonne "iso", devenue inutile
        total = total.drop('iso', axis=1)

        # Et enregistrons le résultat dans un fichier CSV
        if temporalite == "annee":
            nom_fichier = repertoire_matrice+"matrice-"+str(annee_en_cours)+".csv"
        else:
            nom_fichier = repertoire_matrice + "matrice-" + str(debut_periode) +"-"+ str(fin_periode-1)+ ".csv"
        print(nom_fichier)
        total.rename(columns={'NOM_FR':''}).to_csv(nom_fichier, sep=';', index=False)

        # Et on passe à l'annéee suivante
        if temporalite == "annee":
            annee_en_cours = annee_en_cours + 1
        else:
            annee_en_cours = annee_en_cours + intervalle

if __name__ == "__main__":
    load_AGNU()